import {Card, CardGroup, Col, Row, Container } from "react-bootstrap";


export const Superhero = () => {
    return (
<Container id="superhero">
    <div>
        <h1 className="titlefilm">SUPERHERO FILM</h1>
    </div>

    <Row xs={1} lg={3} md={2}>
    <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/ee/fc/7d/eefc7ddac4103ba60c54cd8155ce11b4.jpg" />
            <Card.Body>
            <Card.Title className="text-center">DOCTOR STRANGE</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/1f/71/0b/1f710b6ab3e51c61280c2fab97c6f5a0.jpg" />
            <Card.Body>
            <Card.Title className="text-center">IRON MAN</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/95/39/bb/9539bb24c02f75cd42feaff00f9db74e.jpg" />
            <Card.Body>
            <Card.Title className="text-center">JOKER</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/c0/69/2b/c0692ba704cf3c3239e1042b49a153b0.jpg" />
            <Card.Body>
            <Card.Title className="text-center">CIVIL WAR</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/37/7c/9f/377c9fca0861e6ac72880cf2312b3066.jpg" />
            <Card.Body>
            <Card.Title className="text-center">BATMAN</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/79/76/ec/7976ec0b8d93f8b341e9657b13480f3c.jpg" />
            <Card.Body>
            <Card.Title className="text-center">SUPERMAN</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>
    </Row>
</Container>
)
}

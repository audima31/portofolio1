import {Card, CardGroup, Col, Row, Container } from "react-bootstrap";


export const Trending = () => {
    return (
<Container id="trending">
    <div>
        <h1 className="titlefilm">TRENDING FILM</h1>
    </div>

    <Row xs={1} lg={3} md={2}>
    <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/30/a2/cb/30a2cbc7830110cd118272af60923148.jpg" />
            <Card.Body>
            <Card.Title className="text-center">PARASITE</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/80/06/c4/8006c42dfe474bffa96fdc6d3de89cad.jpg" />
            <Card.Body>
            <Card.Title className="text-center">JAWS</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/a8/2b/b5/a82bb5eadfee5bbe2f1de6e3c16c7391.jpg" />
            <Card.Body>
            <Card.Title className="text-center">TITANIC</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/f7/2e/2f/f72e2ffdb55e60b6b8ece76d9bf8e8c2.jpg" />
            <Card.Body>
            <Card.Title className="text-center">BUDAPEST</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/01/2b/c0/012bc040d22bcb7dbfe01af96ec6758c.jpg" />
            <Card.Body>
            <Card.Title className="text-center">BALLY DRIVER</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>

        <Col xs={4} className="mt-3">
        <Card style={{width: '25em', background: '#1f2227', color: 'white', border: '3px solid white'}}>
            <Card.Img className="foto" variant="top" src="https://i.pinimg.com/236x/19/61/25/1961250a69044a70186de1e481440889.jpg" />
            <Card.Body>
            <Card.Title className="text-center">APOCALYPSE</Card.Title>
            <Card.Text>
                This is a longer card with supporting text below as a natural.
            </Card.Text>
            <Card.Text>
                Last updated 3 minutes ago.
            </Card.Text>
            </Card.Body>
        </Card>
        </Col>
    </Row>
</Container>
)
}

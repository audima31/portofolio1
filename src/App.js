import React from 'react';
import { NavigationBar } from './components/NavigationBar';
import { Intro } from './components/Intro';
import { Trending } from './components/Trending';
import { Superhero } from './components/Superhero';
import { Footer } from './components/Footer';
import "./style/landingPage.css";

function App() {
  return (
    <div>
      {/* Start Intro */}
      <div className='myBg'>
        <NavigationBar />
        <Intro />
      </div>
      {/* End Intro */}

      {/* Start Trending Film */}
      <div className='myBg2'>
        <Trending />
      </div>
      {/* End Trending Film */}

      {/* Start Superhero Film */}
      <div className='myBg3'>
        <Superhero/>
      </div>
      {/* End Superhero Film */}

      {/* Footer */}
        <Footer/>
      {/* End Footer */}
    </div>
  );
}

export default App;

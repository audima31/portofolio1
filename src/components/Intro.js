import {Button, Image} from "react-bootstrap";
import introImage from "../assets/Intro.png"

export const Intro = () => {
    return (
    <div className="container mt-5">
        <div className="title text-center">  
            <div>Nonton Gratis</div>
            <div>Ga Pake Karcis</div>
    
             <div>
            <Button className="button mt-3" style={{background: "#017671", border:"black"}}>Liat Semua List</Button>
            </div>

            <div className="mt-5">
            <Image src={introImage} style={{width: "10em"}} />
            </div>
        </div>
    </div>
    )
}
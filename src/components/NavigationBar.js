import { Navbar, Container, Nav} from 'react-bootstrap';

export const NavigationBar = () => {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand className='fw-bold' style={{color: "white"}}>AudimaFILMS</Navbar.Brand>
                <Nav>
                    <Nav.Link style={{color: "#cfd4ed"}} href="#trending">Trending</Nav.Link>
                    <Nav.Link style={{color: "#cfd4ed"}} href="#superhero">Superhero</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    )
}